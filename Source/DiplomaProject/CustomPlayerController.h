// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "InputCoreTypes.h"
#include "CustomPlayerController.generated.h"

/**
 * 
 */
UCLASS(config = Game)
class DIPLOMAPROJECT_API ACustomPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:

	UFUNCTION(BlueprintCallable, Category = State)
		void ChangeStateTo_Spectator();

	UFUNCTION(BlueprintCallable, Category = State)
		void ChangeStateTo_Player();


};

