// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "DiplomaProject.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, DiplomaProject, "DiplomaProject" );
 