// Fill out your copyright notice in the Description page of Project Settings.


#include "InterplayActor.h"
#include <Components/SceneComponent.h>
#include <Components/BoxComponent.h>
#include "Blueprint/UserWidget.h"
#include <Runtime\UMG\Public\Components\TextBlock.h>
#include <UMG.h>
#include <Kismet/GameplayStatics.h>

// Sets default values
AInterplayActor::AInterplayActor()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	SceneComp = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	RootComponent = SceneComp;

	HitBox = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollision"));
	HitBox->SetupAttachment(SceneComp);
	HitBox->SetGenerateOverlapEvents(true);
	//HitBox->OnComponentBeginOverlap.RemoveDynamic(this, &AInterplayActor::OnOverlapBegin);
	/*MessageWidget = CreateDefaultSubobject<UidgetComponent>(TEXT("MessageWidget"));
	MessageWidget->SetupAttachment(SceneComp);*/

}

// Called when the game starts or when spawned
void AInterplayActor::BeginPlay()
{
	Super::BeginPlay();
	HitBox->OnComponentBeginOverlap.AddDynamic(this, &AInterplayActor::OnOverlapBegin);
	HitBox->OnComponentEndOverlap.AddDynamic(this, &AInterplayActor::OnOverlapEnd);

}

void AInterplayActor::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor == UGameplayStatics::GetPlayerCharacter(GetWorld(), 0))
	{
		if (MessageWidget_Class)
		{
			MessageWidget = CreateWidget<UUserWidget>(GetWorld()->GetFirstPlayerController(), MessageWidget_Class);
			if (MessageWidget)
			{
				MessageWidget->AddToViewport();
				bWidgetInViewport = true;
			}
		}
	}
}

void AInterplayActor::OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor == UGameplayStatics::GetPlayerCharacter(GetWorld(), 0))
	{
		if (MessageWidget)
		{
			MessageWidget->RemoveFromParent();
		}
	}
}

// Called every frame
void AInterplayActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}