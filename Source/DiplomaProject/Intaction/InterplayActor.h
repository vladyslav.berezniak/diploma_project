// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "InterplayActor.generated.h"

UCLASS()
class DIPLOMAPROJECT_API AInterplayActor : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AInterplayActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class USceneComponent* SceneComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UBoxComponent* HitBox;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Message")
		FText ShowMessage;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Widget")
		TSubclassOf<class UUserWidget> MessageWidget_Class;

	//UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Widget")
		class UUserWidget* MessageWidget;

		bool bWidgetInViewport = false;

public:
	UFUNCTION(BlueprintCallable)
		virtual void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION(BlueprintCallable)
		virtual void OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	// Called every frame
	virtual void Tick(float DeltaTime) override;
};
