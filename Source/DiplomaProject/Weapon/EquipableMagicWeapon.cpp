// Fill out your copyright notice in the Description page of Project Settings.


#include "EquipableMagicWeapon.h"
#include "Ability/BaseAbilityComponent.h"
#include <GameFramework/Actor.h>
#include "DiplomaProjectCharacter.h"
#include "EquipableBaseWeapon.h"
#include <Engine/StaticMeshActor.h>
#include <Kismet/KismetMathLibrary.h>
#include <GameFramework/GameSession.h>
#include <GameFramework/HUD.h>
#include <Kismet/KismetSystemLibrary.h>
#include <Private/KismetTraceUtils.h>
#include "DiplomaProjectCharacter.h"
#include <Camera/CameraComponent.h>
#include "AI/BotAI.h"
#include "AI/BotAIController.h"
#include "BasicProjectile.h"
#include "ImpactEffectComponent.h"
#include <Kismet/GameplayStatics.h>


void AEquipableMagicWeapon::SpawnProjectile()
{
	if (Projectile_Class)
	{
		FTransform TmpTransform = GetMuzzleTransform();

		FVector AimLoc = OverlappedChar->GetFollowCamera()->GetComponentLocation() + (OverlappedChar->GetFollowCamera()->GetForwardVector() * 2000.f);
		FRotator MuzzleRotation = UKismetMathLibrary::FindLookAtRotation(TmpTransform.GetLocation(), AimLoc);
		TmpTransform.SetRotation(MuzzleRotation.Quaternion());
		//DrawDebugLine(GetWorld(), TmpTransform.GetLocation(), AimLoc, FColor::Red, true, 5.f);
		ABasicProjectile* TmpProj = Cast<ABasicProjectile>(GetWorld()->SpawnActor(Projectile_Class, &TmpTransform));
		TmpProj->OnHitFire.BindDynamic(ImpactEffectComp, &UImpactEffectComponent::SpawnImpactEffect);
		
		if (MuzzleEmitter)
		{
			UGameplayStatics::SpawnEmitterAtLocation(this, MuzzleEmitter, TmpTransform.GetLocation(), TmpTransform.GetRotation().Rotator(), true);
		}
	}
}

void AEquipableMagicWeapon::SpawnProjectileAI()
{
	if (Projectile_Class)
	{
		FTransform TmpTransform = GetMuzzleTransform();
		ABotAI* OwnerAI = Cast<ABotAI>(GetOwner());
		if (OwnerAI)
		{
			ABotAIController* OwnerController = Cast<ABotAIController>(OwnerAI->GetController());

			FVector AimLoc = (OwnerController->GetEnemy()) ? OwnerController->GetEnemy()->GetActorLocation() : OwnerAI->GetActorForwardVector() * 2000.f;

			FRotator ProjectileRotation = UKismetMathLibrary::FindLookAtRotation(TmpTransform.GetLocation(), AimLoc);

			TmpTransform.SetRotation(ProjectileRotation.Quaternion());
			//DrawDebugLine(GetWorld(), TmpTransform.GetLocation(), AimLoc, FColor::Red, true, 5.f);
		}
		ABasicProjectile* TmpProj = Cast<ABasicProjectile>(GetWorld()->SpawnActor(Projectile_Class, &TmpTransform));
		TmpProj->OnHitFire.BindDynamic(ImpactEffectComp, &UImpactEffectComponent::SpawnImpactEffect);

		if (MuzzleEmitter)
		{
			UGameplayStatics::SpawnEmitterAtLocation(this, MuzzleEmitter, TmpTransform.GetLocation(), TmpTransform.GetRotation().Rotator(), true);
		}
	}
}

FTransform AEquipableMagicWeapon::GetMuzzleTransform()
{
	if (MuzzleSocketName.IsNone())
	{
		FMessageDialog::Debugf(NSLOCTEXT("Error", "Key", "MuzzleSoketName is none, initialize"));
		return FTransform::Identity;
	}
	return PickupMesh->GetSocketTransform(MuzzleSocketName);
}
