// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include <DelegateCombinations.h>
#include "BasicProjectile.generated.h"

DECLARE_DYNAMIC_DELEGATE_OneParam(FNewHitResultDelegate, FHitResult, Hit);

UCLASS()
class DIPLOMAPROJECT_API ABasicProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABasicProjectile();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class USphereComponent* HitSphere;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Effects")
		class UParticleSystemComponent* TailEmitter;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UProjectileMovementComponent* ProjectileMovementComp;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "ProjectileMovement")
		float StartSpeed = 1700.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "ProjectileMovement")
		float MaxSpeed = 2500.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "ProjectileMovement")
		float GravityScale = 0.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "ProjectileMovement")
		float DestroyDelay = 10.f;


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY()
	FNewHitResultDelegate OnHitFire;

	virtual void NotifyHit(class UPrimitiveComponent* MyComp, AActor* Other, class UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit) override;

};
