// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "InventorySystem/EquipablePickup.h"
#include "EquipableBaseWeapon.generated.h"

/**
 * 
 */
UENUM(BlueprintType)
enum class EWeaponTypeEnum : uint8
{
	DE_Melee 			UMETA(DisplayName = "Melee"),
	DE_Ranged 			UMETA(DisplayName = "Ranged"),
	DE_Magic			UMETA(DisplayName = "Magic")
};

UCLASS()
class DIPLOMAPROJECT_API AEquipableBaseWeapon : public AEquipablePickup
{
	GENERATED_BODY()

public:
	AEquipableBaseWeapon();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	EWeaponTypeEnum WeaponEnumType;

	UFUNCTION(BlueprintCallable)
	virtual void Attack();

	UFUNCTION(BlueprintCallable)
	virtual void StartAttack();

	UFUNCTION(BlueprintCallable)
	virtual void StopAttack();

	UFUNCTION(BlueprintCallable)
	virtual void MontagePlay(class ADiplomaProjectCharacter* CharOwner, class UAnimMontage* MontageToPlay);

	virtual void OnAnimationEnded(class UAnimMontage* Montage, bool bInterrupted);

	virtual void OnPickupUnEquipped() override;

	void NewHit(FHitResult Hit);

protected:

	UPROPERTY(VisibleAnywhere)
	class UImpactEffectComponent* ImpactEffectComp;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	class UAnimMontage* AttackMontage;

	virtual void OnPickupUse() override;

	FTimerHandle Attack_TH;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	float AttackPointsCost;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	bool bIsChargable = false;

	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	bool bAutoAttack = false;

	UPROPERTY(EditDefaultsOnly, Category = "Weapon", meta = (EditCondition = "bAutoAttack"))
	float AttackRate = 125;

	UPROPERTY(EditDefaultsOnly, Category = "Weapon", meta = (EditCondition = "bIsChargable"))
	float ChargeTime = 0.0f;

};
