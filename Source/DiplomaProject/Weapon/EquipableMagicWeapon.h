// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapon/EquipableBaseWeapon.h"
#include "EquipableMagicWeapon.generated.h"

/**
 * 
 */
UCLASS()
class DIPLOMAPROJECT_API AEquipableMagicWeapon : public AEquipableBaseWeapon
{
	GENERATED_BODY()
	
protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
		TSubclassOf<class ABasicProjectile>	Projectile_Class;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
		FName	MuzzleSocketName = TEXT("Muzzle");

	UFUNCTION(BlueprintCallable)
		FTransform	GetMuzzleTransform();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
		class UParticleSystem* MuzzleEmitter;

public:
	
	UFUNCTION(BlueprintCallable)
	void	SpawnProjectile();

	UFUNCTION(BlueprintCallable)
	void	SpawnProjectileAI();
};
