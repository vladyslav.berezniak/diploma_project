// Fill out your copyright notice in the Description page of Project Settings.


#include "BasicProjectile.h"
#include <GameFramework/ProjectileMovementComponent.h>
#include <Components/StaticMeshComponent.h>
#include <Particles/ParticleSystemComponent.h>
#include <Components/AudioComponent.h>
#include <Kismet/GameplayStatics.h>
#include "ImpactEffectActor.h"
#include <Engine/World.h>
#include <Components/SphereComponent.h>

// Sets default values


ABasicProjectile::ABasicProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	/*SceneComp = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	RootComponent = SceneComp;*/

	//StaticMeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Projectile"));
	//StaticMeshComp->SetupAttachment(SceneComp);

	HitSphere = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionSphere"));
	HitSphere->InitSphereRadius(5.0f);
	HitSphere->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	HitSphere->SetCollisionResponseToAllChannels(ECR_Ignore);
	HitSphere->SetCollisionResponseToChannel(ECC_WorldStatic, ECR_Block);
	HitSphere->SetCollisionResponseToChannel(ECC_WorldDynamic, ECR_Block);
	HitSphere->SetCollisionResponseToChannel(ECC_Pawn, ECR_Block);
	RootComponent = HitSphere;

	ProjectileMovementComp = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovement"));
	ProjectileMovementComp->InitialSpeed = StartSpeed;
	ProjectileMovementComp->MaxSpeed = MaxSpeed;
	ProjectileMovementComp->ProjectileGravityScale = GravityScale;

	TailEmitter = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("TailEmitter"));
	TailEmitter->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ABasicProjectile::BeginPlay()
{
	Super::BeginPlay();
	SetLifeSpan(DestroyDelay);
	
}

// Called every frame
void ABasicProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABasicProjectile::NotifyHit(UPrimitiveComponent* MyComp, AActor* Other, UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit)
{
	Super::NotifyHit(MyComp, Other, OtherComp, bSelfMoved, HitLocation, HitNormal, NormalImpulse, Hit);

	if (OnHitFire.IsBound())
	{
		OnHitFire.Execute(Hit);
	}
	Destroy();
}

