// Fill out your copyright notice in the Description page of Project Settings.


#include "EquipableBaseWeapon.h"
#include <Animation/AnimMontage.h>
#include "DiplomaProjectCharacter.h"
#include "Ability/BaseAbilityComponent.h"
#include <GameFramework/Actor.h>
#include "ImpactEffectComponent.h"


AEquipableBaseWeapon::AEquipableBaseWeapon()
{
	ImpactEffectComp = CreateDefaultSubobject<UImpactEffectComponent>(TEXT("ImpactEffectComp"));
}

void AEquipableBaseWeapon::Attack()
{
	switch (WeaponEnumType)
	{
	case EWeaponTypeEnum::DE_Melee:
		if (OverlappedChar->GetAbilityComponent(EAbilityTypeEnum::DE_Stamina)->GetCurrentAbilityPoints() > AttackPointsCost)
		{
			OverlappedChar->GetAbilityComponent(EAbilityTypeEnum::DE_Stamina)->RemoveAbilityPoints(AttackPointsCost);
			MontagePlay(OverlappedChar, AttackMontage);
		}
		break;
	case EWeaponTypeEnum::DE_Ranged:
		if (OverlappedChar->GetAbilityComponent(EAbilityTypeEnum::DE_Stamina)->GetCurrentAbilityPoints() > AttackPointsCost)
		{
			OverlappedChar->GetAbilityComponent(EAbilityTypeEnum::DE_Stamina)->RemoveAbilityPoints(AttackPointsCost);
			MontagePlay(OverlappedChar, AttackMontage);
		}
		break;
	case EWeaponTypeEnum::DE_Magic:
		if (OverlappedChar->GetAbilityComponent(EAbilityTypeEnum::DE_Mana)->GetCurrentAbilityPoints() > AttackPointsCost)
		{
			OverlappedChar->GetAbilityComponent(EAbilityTypeEnum::DE_Mana)->RemoveAbilityPoints(AttackPointsCost);
			MontagePlay(OverlappedChar, AttackMontage);
		}
		break;
	default:
		GEngine->AddOnScreenDebugMessage(-1, 5.0, FColor::Red, TEXT("Wrong Weapon Type"));
		break;
	}

}

void AEquipableBaseWeapon::StartAttack()
{
	GetWorldTimerManager().SetTimer(Attack_TH, this, &AEquipableBaseWeapon::Attack, 60 / AttackRate, bAutoAttack, ChargeTime);
}

void AEquipableBaseWeapon::StopAttack()
{
	GetWorldTimerManager().ClearTimer(Attack_TH);
}

void AEquipableBaseWeapon::MontagePlay(ADiplomaProjectCharacter* CharOwner, UAnimMontage* MontageToPlay)
{
	if (MontageToPlay)
	{
		if (CharOwner)
		{
			if (!CharOwner->GetMesh()->GetAnimInstance()->Montage_IsPlaying(MontageToPlay))
			{
				FOnMontageEnded EndDelegate;
				EndDelegate.BindUObject(this, &AEquipableBaseWeapon::OnAnimationEnded);
				CharOwner->GetMesh()->GetAnimInstance()->Montage_Play(MontageToPlay);
				CharOwner->GetMesh()->GetAnimInstance()->Montage_SetEndDelegate(EndDelegate);
			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("No Owner found"));
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("No montage"));
	}
}

void AEquipableBaseWeapon::OnAnimationEnded(UAnimMontage* Montage, bool bInterrupted)
{
	//do something...
}

void AEquipableBaseWeapon::OnPickupUnEquipped()
{
	if (OverlappedChar->CurrentWeapon == this)
	{
		OverlappedChar->CurrentWeapon = nullptr;
	}
	Super::OnPickupUnEquipped();
}

void AEquipableBaseWeapon::OnPickupUse()
{
	OverlappedChar->CurrentWeapon = this;
	Super::OnPickupUse();
}
