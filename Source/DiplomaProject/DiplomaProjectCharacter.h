// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "Core.h"
#include "GameFramework/Character.h"
#include "DiplomaProjectCharacter.generated.h"

UENUM(BlueprintType)
enum class EAbilityTypeEnum : uint8
{
	DE_Health 			UMETA(DisplayName = "Health"),
	DE_Mana 			UMETA(DisplayName = "Mana"),
	DE_Stamina			UMETA(DisplayName = "Stamina")
};


UCLASS(config=Game)
class ADiplomaProjectCharacter : public ACharacter
{
	GENERATED_BODY()

protected:
	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Ability")
	EAbilityTypeEnum AbilityEnumType;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "ModularChar", meta = (AllowPrivateAccess = "true"))
	class USkeletalMeshComponent* FaceMesh;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "ModularChar", meta = (AllowPrivateAccess = "true"))
	class USkeletalMeshComponent* GlovesMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "ModularChar", meta = (AllowPrivateAccess = "true"))
	class USkeletalMeshComponent* ShoesMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "ModularChar", meta = (AllowPrivateAccess = "true"))
	class USkeletalMeshComponent* HairMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Inventory")
	class UInventoryComponent* InventoryComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Equipment")
	class UEquipmentComponent* EquipmentComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Health")
	class UHealthComponent* HealthComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Mana")
	class UManaComponent* ManaComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Stamina")
	class UStaminaComponent* StaminaComp;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Inventory")
	class UAnimMontage* PickupMontage;

	//UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Inventory")
	bool	bCanInteract = false;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "CharacterMovement:Walking")
	float	WalkSpeed = 600.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "CharacterMovement:Walking")
	float	SprintSpeed = 1000.0f;

	UPROPERTY(EditAnywhere, Category = Camera)
		float ArmMultiplier = 10.f;

	UPROPERTY(EditAnywhere, Category = Camera)
		float MaxArmLengh = 1500.f;

	UPROPERTY(EditAnywhere, Category = Camera)
		float MinArmLengh = 400.f;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Inventory")
	TSubclassOf<class UUserWidget> InventoryWidget_Class;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Equipment")
	TSubclassOf<class UUserWidget> EquipmentWidget_Class;
	
	//UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Interact)
	class ABasePickup* PickupItem = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	TSubclassOf<class AEquipableBaseWeapon> Weapon_Class;

	UFUNCTION(BlueprintCallable)
	AEquipableBaseWeapon* SpawnWeapon();
	UFUNCTION(BlueprintCallable)
	void AttachWeapon(FName SocketName);

	class UUserWidget* EquipmentWidget;
	class UUserWidget* InventoryWidget;
	
	bool bIsInventoryOpened = false;
	bool bIsEquipmentOpened = false;


	UFUNCTION(BlueprintCallable)
	void	SaveGame();
	
	UFUNCTION(BlueprintCallable)
	void	LoadGame();
	
	UFUNCTION(BlueprintCallable)
	void	SaveGameToSlot(FString SlotName = "DefaultSaveSlot", int32 UserIndex = 0);

	UFUNCTION(BlueprintCallable)
	void	LoadGameFromSlot(FString SlotName = "DefaultSaveSlot", int32 UserIndex = 0);

	void	SaveBodyParts(class UDiplomaSaveGame* GameSaveInstance);
	void	LoadBodyParts(class UDiplomaSaveGame* GameSaveInstance);
	void	SaveAbilityData(class UDiplomaSaveGame* GameSaveInstance, class UBaseAbilityComponent* AbilityToSave);
	void	LoadAbilityData(class UDiplomaSaveGame* GameSaveInstance, class UBaseAbilityComponent* AbilityToLoad);
	void	CameraZoom(float Scale);
public:
	ADiplomaProjectCharacter();
	
	UFUNCTION(BlueprintCallable)
	virtual	void	OnPlayerDied();

	UPROPERTY(BlueprintReadWrite)
	class AEquipableBaseWeapon* CurrentWeapon;
	
	bool bCanJump = true;

	bool bCanRoll = true;

	UFUNCTION(BlueprintCallable)
	void	Equip(FName EquipmentSocket, class AActor* EquipmentActor);
	
	void	SetPickupItem(ABasePickup* PickupToSet);

	void	SetCanInteract(bool bInteract);
	
	UFUNCTION(BlueprintCallable)
	void	StartSprint();

	UFUNCTION(BlueprintCallable)
	void	StopSprint();

	virtual void Jump() override;
	virtual void StopJumping() override;

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseLookUpRate;

	UFUNCTION(BlueprintCallable)
	void	OnInventoryOpen();
	
	UFUNCTION(BlueprintCallable)
	void	OnEquipmentOpen();

	UFUNCTION(BlueprintCallable)
	void	StartAttack();

	UFUNCTION(BlueprintCallable)
	void	StopAttack();

	UFUNCTION(BlueprintPure)
	bool	IsAlive() const;

protected:
	
	/** Called for forwards/backward input */
	void	MoveForward(float Value);

	/** Called for side to side input */
	void	MoveRight(float Value);

	/** 
	 * Called via input to turn at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void	TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void	LookUpAtRate(float Rate);

	UFUNCTION(BlueprintCallable)
	virtual void	OnInteract();

	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

	UFUNCTION(BlueprintPure)
	UInventoryComponent* GetInventoryComponent() const { return InventoryComp; };

	UFUNCTION(BlueprintPure)
	UEquipmentComponent* GetEquipmentComponent() const { return EquipmentComp; }

	UFUNCTION(BlueprintPure)
	class UBaseAbilityComponent* GetAbilityComponent(EAbilityTypeEnum AbilityEnum);
	 
	UFUNCTION(BlueprintPure)
	class AEquipableBaseWeapon* GetCurrentWeapon() const { return CurrentWeapon; }

	virtual	void	BeginPlay() override;
};

