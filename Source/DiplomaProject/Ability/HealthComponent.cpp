// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthComponent.h"
#include "DiplomaProjectCharacter.h"
#include "CustomPlayerController.h"
#include <Kismet/GameplayStatics.h>

void UHealthComponent::DamageHandle(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	if (bCanTakeDamage)
	{
		if (CurrentAbilityPoints > 0)
		{
			RemoveAbilityPoints(Damage);
		}
		else
		{
			bCanTakeDamage = false;
		}
	} 
	
}

void UHealthComponent::OnAbilityZeroFunc()
{
	if (bCanTakeDamage)
	{
		Super::OnAbilityZeroFunc();
	}
	bCanTakeDamage = false;
}

void UHealthComponent::BeginPlay()
{
	if (GetOwner())
	{
		GetOwner()->OnTakeAnyDamage.AddDynamic(this, &UHealthComponent::DamageHandle);
	}
	Super::BeginPlay();
}
