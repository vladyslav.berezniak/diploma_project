// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include <DelegateCombinations.h>
#include "BaseAbilityComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FAbilityNoParamDelegate);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DIPLOMAPROJECT_API UBaseAbilityComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UBaseAbilityComponent();

protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Ability")
	float MaxAbilityPoints = 100;

	//UPROPERTY(EditDefaultsOnly, Category = Ability)
	float CurrentAbilityPoints;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Ability", meta = (EditCondition = "bCanRecover", DisplayName = "Points to recover in 1 step"))
	float RecoverStepPoints;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Ability", meta = (EditCondition = "bCanRecover", DisplayName = "Recover step time in sec's"))
	float RecoverStepTime;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Ability", meta = (EditCondition = "bCanRecover", DisplayName = "First recover delay in sec's"))
	float RecoverDelay;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Ability")
	bool bCanRecover = true;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Ability", meta = (DisplayName = "Play animation on ability = 0"))
	bool bOnAbilityZero = false;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Ability", meta = (EditCondition = "bOnAbilityZero"))
	class UAnimationAsset* AbilityZeroAnim;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Ability", meta = (EditCondition = "bOnAbilityZero"))
	bool bLoopAnimation = false;


	bool bIsLosingPoints;

	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	
	FTimerHandle Timer_TH;

	UFUNCTION(BlueprintPure)
	bool IsRecoverable() const { return bCanRecover; };

	UFUNCTION(BlueprintPure)
	float GetCurrentAbilityPoints() const { return CurrentAbilityPoints; };
	
	UFUNCTION(BlueprintPure)
	float GetMaxAbilityPoints() const { return MaxAbilityPoints; };

	UFUNCTION(BlueprintPure)
	float GetRecoverStepPoints() const { return RecoverStepPoints; };

	UFUNCTION(BlueprintPure)
	float GetRecoverDelay() const { return RecoverDelay; };

	UFUNCTION(BlueprintCallable)
	void SetMaxAbilityPoints(float NewMaxPoints) { MaxAbilityPoints = NewMaxPoints; OnAbilityChanged.Broadcast(); };

	UFUNCTION(BlueprintCallable)
	void SetCurrentAbilityPoints(float NewCurrentPoints) { CurrentAbilityPoints = NewCurrentPoints; OnAbilityChanged.Broadcast();};

	UFUNCTION(BlueprintCallable)
	void SetRecoverDelay(float NewRecoverDelay) { RecoverDelay = NewRecoverDelay; };

	UFUNCTION(BlueprintCallable)
	void SetRecoverStepPoints(float NewRecoverStepPoints) { RecoverStepPoints = NewRecoverStepPoints; };
	
	UFUNCTION(BlueprintCallable)
	virtual void Recover();
	
	UFUNCTION(BlueprintCallable)
	virtual void StartRecover();
	
	UFUNCTION(BlueprintCallable)
	virtual void StopRecover();
	
	UFUNCTION(BlueprintCallable)
	virtual void StartLosePoints(float PointsToLose);
	
	UFUNCTION(BlueprintCallable)
	virtual void StopLosePoints();
	
	UFUNCTION(BlueprintCallable)
	virtual void AddAbilityPoints(float PointsToAdd);
	
	UFUNCTION(BlueprintCallable)
	virtual void RemoveAbilityPoints(float PointsToRemove);

	UFUNCTION(BlueprintCallable)
	virtual void OnAbilityZeroFunc();

	UPROPERTY(BlueprintAssignable, Category = "Ability")
	FAbilityNoParamDelegate OnAbilityChanged;

	UPROPERTY(BlueprintAssignable, Category = "Ability")
	FAbilityNoParamDelegate OnAbilityZero;
		
};
