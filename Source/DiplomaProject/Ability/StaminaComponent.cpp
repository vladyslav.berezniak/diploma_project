// Fill out your copyright notice in the Description page of Project Settings.


#include "StaminaComponent.h"

void UStaminaComponent::AddAbilityPoints(float PointsToAdd)
{
	Super::AddAbilityPoints(PointsToAdd);
}

void UStaminaComponent::RemoveAbilityPoints(float PointsToRemove)
{
	Super::RemoveAbilityPoints(PointsToRemove);
	if (CurrentAbilityPoints == 0)
	{
		GetOwner()->GetWorldTimerManager().ClearTimer(Timer_TH);
		StartRecover();
	}
}

void UStaminaComponent::StartRecover()
{
	Super::StartRecover();
}

void UStaminaComponent::StopRecover()
{
	Super::StartRecover();
}

void UStaminaComponent::BeginPlay()
{	
	Super::BeginPlay();
}
