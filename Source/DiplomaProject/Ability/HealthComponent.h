// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Ability/BaseAbilityComponent.h"
#include "HealthComponent.generated.h"

/**
 * 
 */
UCLASS()
class DIPLOMAPROJECT_API UHealthComponent : public UBaseAbilityComponent
{
	GENERATED_BODY()
	
protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Ability)
	bool	bCanTakeDamage = true;

	UFUNCTION()
	void DamageHandle(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

	virtual void OnAbilityZeroFunc() override;
	
	UFUNCTION()
	virtual void BeginPlay() override;

};
