// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Ability/BaseAbilityComponent.h"
#include "StaminaComponent.generated.h"

/**
 * 
 */
UCLASS()
class DIPLOMAPROJECT_API UStaminaComponent : public UBaseAbilityComponent
{
	GENERATED_BODY()
	
public:

	virtual void AddAbilityPoints(float PointsToAdd) override;

	virtual void RemoveAbilityPoints(float PointsToRemove) override;

	virtual void StartRecover() override;

	virtual void StopRecover() override;

	virtual void BeginPlay() override;

};
