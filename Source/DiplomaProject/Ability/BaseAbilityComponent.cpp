// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseAbilityComponent.h"
#include <TimerManager.h>
#include <Animation/AnimMontage.h>
#include "DiplomaProjectCharacter.h"
#include <GameFramework/CharacterMovementComponent.h>
#include <Animation/AnimationAsset.h>
#include <Components/SkeletalMeshComponent.h>


// Sets default values for this component's properties
UBaseAbilityComponent::UBaseAbilityComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;
	// ...
	OnAbilityZero.AddDynamic(this, &UBaseAbilityComponent::OnAbilityZeroFunc);
}


// Called when the game starts
void UBaseAbilityComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	CurrentAbilityPoints = MaxAbilityPoints;
	OnAbilityChanged.Broadcast();
}


// Called every frame
void UBaseAbilityComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}


void UBaseAbilityComponent::Recover()
{
	float NewCurrentAbilityPoints = CurrentAbilityPoints + RecoverStepPoints;
	if (NewCurrentAbilityPoints < MaxAbilityPoints && CurrentAbilityPoints >= 0)
	{
		if (CurrentAbilityPoints == 0)
		{
			ADiplomaProjectCharacter* Owner = Cast<ADiplomaProjectCharacter>(GetOwner());
			Owner->GetCharacterMovement()->SetMovementMode(MOVE_Walking);
			Owner->GetMesh()->SetAnimationMode(EAnimationMode::AnimationBlueprint);
			Owner->bUseControllerRotationYaw = true;
		}
		CurrentAbilityPoints = NewCurrentAbilityPoints;
		GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Green, GetFName().ToString() + FString::FromInt(CurrentAbilityPoints));

	}
	else
	{
		CurrentAbilityPoints = MaxAbilityPoints;
		StopRecover();
	}
	OnAbilityChanged.Broadcast();
}

void UBaseAbilityComponent::StartRecover()
{
	if (bCanRecover && !bIsLosingPoints)
	{
		GetOwner()->GetWorldTimerManager().SetTimer(Timer_TH, this, &UBaseAbilityComponent::Recover, RecoverStepTime, true, RecoverDelay);
	}
}

void UBaseAbilityComponent::StopRecover()
{
	GetOwner()->GetWorldTimerManager().ClearTimer(Timer_TH);
}

void UBaseAbilityComponent::StartLosePoints(float PointsToLose)
{
	FTimerDelegate LosePoints_TD;
	LosePoints_TD.BindUFunction(this, FName("RemoveAbilityPoints"), PointsToLose);
	GetWorld()->GetTimerManager().SetTimer(Timer_TH, LosePoints_TD, 2.0, true);
	bIsLosingPoints = true;
}

void UBaseAbilityComponent::StopLosePoints()
{
	bIsLosingPoints = false;
	GetWorld()->GetTimerManager().ClearTimer(Timer_TH);
	StartRecover();
}

void UBaseAbilityComponent::AddAbilityPoints(float PointsToAdd)
{
	if (CurrentAbilityPoints > 0 && CurrentAbilityPoints != MaxAbilityPoints)
	{
		float NewAbilityPoints = CurrentAbilityPoints + PointsToAdd;
		if (NewAbilityPoints < MaxAbilityPoints)
		{
			CurrentAbilityPoints = NewAbilityPoints;
		}
		else
		{
			CurrentAbilityPoints = MaxAbilityPoints;
		}
		OnAbilityChanged.Broadcast();
	}
}

void UBaseAbilityComponent::RemoveAbilityPoints(float PointsToRemove)
{
	if (CurrentAbilityPoints > 0)
	{
		float NewAbilityPoints = CurrentAbilityPoints - PointsToRemove;
		if (NewAbilityPoints <= 0)
		{
			CurrentAbilityPoints = 0;
			OnAbilityZero.Broadcast();
		}
		else
		{
			CurrentAbilityPoints = NewAbilityPoints;
		}
		OnAbilityChanged.Broadcast();
		StartRecover();
	}
}

void UBaseAbilityComponent::OnAbilityZeroFunc()
{
	if (bOnAbilityZero)
	{
		ADiplomaProjectCharacter* Owner = Cast<ADiplomaProjectCharacter>(GetOwner());
		Owner->GetCharacterMovement()->DisableMovement();
		Owner->bUseControllerRotationYaw = false;

		if (AbilityZeroAnim)
		{
			Owner->GetMesh()->PlayAnimation(AbilityZeroAnim, bLoopAnimation);
			GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Green, TEXT("Ability Zero"));
		}
	}
}

