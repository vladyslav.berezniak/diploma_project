// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "DiplomaProjectGameMode.generated.h"

UCLASS(minimalapi)
class ADiplomaProjectGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ADiplomaProjectGameMode();

	virtual void BeginPlay() override;
};



