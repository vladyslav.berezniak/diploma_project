// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "DiplomaProjectCharacter.h"
//#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "InventorySystem/InventoryComponent.h"
#include "Ability/HealthComponent.h"
#include "Ability/ManaComponent.h"
#include "Ability/StaminaComponent.h"
#include <UserWidget.h>
#include <Kismet/KismetMathLibrary.h>
#include <GameFramework/PlayerController.h>
#include <MessageDialog.h>
#include "DiplomaSaveGame.h"
#include <Kismet/GameplayStatics.h>
#include "Weapon/EquipableBaseWeapon.h"
#include "InventorySystem/EquipmentComponent.h"
#include "CustomPlayerController.h"


//////////////////////////////////////////////////////////////////////////
// ADiplomaProjectCharacter


ADiplomaProjectCharacter::ADiplomaProjectCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	USkeletalMeshComponent* BaseMesh = GetMesh();
	BaseMesh->ComponentTags.Add(FName("Body"));

	FaceMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Face"));
	FaceMesh->SetupAttachment(BaseMesh);
	FaceMesh->SetMasterPoseComponent(BaseMesh);
	FaceMesh->ComponentTags.Add(FName("Face"));

	GlovesMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Gloves"));
	GlovesMesh->SetupAttachment(BaseMesh);
	GlovesMesh->SetMasterPoseComponent(BaseMesh);
	GlovesMesh->ComponentTags.Add(FName("Gloves"));

	ShoesMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Shoes"));
	ShoesMesh->SetupAttachment(BaseMesh);
	ShoesMesh->SetMasterPoseComponent(BaseMesh);
	ShoesMesh->ComponentTags.Add(FName("Shoes"));

	HairMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Hair"));
	HairMesh->SetupAttachment(BaseMesh);
	HairMesh->SetMasterPoseComponent(BaseMesh);
	HairMesh->ComponentTags.Add(FName("Hair"));


	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->MaxWalkSpeed = WalkSpeed;
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	InventoryComp = CreateDefaultSubobject<UInventoryComponent>(TEXT("Inventory"));
	EquipmentComp = CreateDefaultSubobject<UEquipmentComponent>(TEXT("Equipment"));
	HealthComp = CreateDefaultSubobject<UHealthComponent>(TEXT("HealthComponent"));
	ManaComp = CreateDefaultSubobject<UManaComponent>(TEXT("ManaComponent"));
	StaminaComp = CreateDefaultSubobject<UStaminaComponent>(TEXT("StaminaComponent"));

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)
}



//////////////////////////////////////////////////////////////////////////
// Input

void ADiplomaProjectCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);
	PlayerInputComponent->BindAction("Interact", IE_Pressed, this, &ADiplomaProjectCharacter::OnInteract);
	PlayerInputComponent->BindAction("Inventory", IE_Pressed, this, &ADiplomaProjectCharacter::OnInventoryOpen);
	PlayerInputComponent->BindAction("Equipment", IE_Pressed, this, &ADiplomaProjectCharacter::OnEquipmentOpen);
	PlayerInputComponent->BindAction("Sprint", IE_Pressed, this, &ADiplomaProjectCharacter::StartSprint);
	PlayerInputComponent->BindAction("Sprint", IE_Released, this, &ADiplomaProjectCharacter::StopSprint);
	PlayerInputComponent->BindAction("SaveGame", IE_Pressed, this, &ADiplomaProjectCharacter::SaveGame);
	PlayerInputComponent->BindAction("LoadGame", IE_Pressed, this, &ADiplomaProjectCharacter::LoadGame);
	PlayerInputComponent->BindAction("Attack", IE_Pressed, this, &ADiplomaProjectCharacter::StartAttack);
	PlayerInputComponent->BindAction("Attack", IE_Released, this, &ADiplomaProjectCharacter::StopAttack);

	PlayerInputComponent->BindAxis("MoveForward", this, &ADiplomaProjectCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ADiplomaProjectCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &ADiplomaProjectCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ADiplomaProjectCharacter::LookUpAtRate);
	PlayerInputComponent->BindAxis(TEXT("CameraZoom"), this, &ADiplomaProjectCharacter::CameraZoom);
}

void ADiplomaProjectCharacter::BeginPlay()
{
	HealthComp->OnAbilityZero.AddDynamic(this, &ADiplomaProjectCharacter::OnPlayerDied);
	FollowCamera->SetPostProcessBlendWeight(0.f);
	Super::BeginPlay();
}

void ADiplomaProjectCharacter::OnPlayerDied()
{
	ACustomPlayerController* CustomContr = Cast<ACustomPlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
	CustomContr->ChangeStateTo_Spectator();
	SetLifeSpan(2.f);
}

void ADiplomaProjectCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ADiplomaProjectCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void ADiplomaProjectCharacter::OnInteract()
{
	if (bCanInteract)
	{
		if (PickupItem != nullptr)
		{
			FVector PickupLocation = PickupItem->GetActorLocation();
			PickupItem->OnPickup(this);
			if (InventoryComp->bIsPickupSuccesful)
			{
				FRotator PlayerRot = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), PickupLocation);
				SetActorRotation(FRotator(0, PlayerRot.Yaw, 0));
				//PickupItem->SetPickupUsability(false);
				if (PickupMontage)
				{
					PlayAnimMontage(PickupMontage);
				}
			}
		}
	}
}

void ADiplomaProjectCharacter::OnInventoryOpen()
{
	if (InventoryComp)
	{
		APlayerController* PController = GetWorld()->GetFirstPlayerController();
		if (InventoryWidget_Class)
		{
			if (!bIsInventoryOpened)
			{
				InventoryWidget = CreateWidget<UUserWidget>(PController, InventoryWidget_Class);
				InventoryWidget->AddToViewport();
				PController->SetInputMode(FInputModeGameAndUI());
				PController->bShowMouseCursor = true;
				GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Red, TEXT("InventoryOpened"));
				InventoryComp->OnInventoryChanged.Broadcast();
				bIsInventoryOpened = true;
			}
			else
			{
				InventoryWidget->RemoveFromParent();
				PController->SetInputMode(FInputModeGameOnly());
				PController->bShowMouseCursor = false;
				GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Red, TEXT("InventoryClosed"));
				bIsInventoryOpened = false;
			}
		}
		else
		{
			FMessageDialog::Debugf(NSLOCTEXT("Error", "Key", "Set Inventory Widget class in Char"));
		}
	}
}

void ADiplomaProjectCharacter::OnEquipmentOpen()
{
	if (EquipmentComp)
	{
		APlayerController* PController = GetWorld()->GetFirstPlayerController();
		if (EquipmentWidget_Class)
		{
			if (!bIsEquipmentOpened)
			{
				EquipmentWidget = CreateWidget<UUserWidget>(PController, EquipmentWidget_Class);
				EquipmentWidget->AddToViewport();
				PController->SetInputMode(FInputModeGameAndUI());
				PController->bShowMouseCursor = true;
				GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Red, TEXT("EquipmentOpened"));
				bIsEquipmentOpened = true;
			}
			else
			{
				EquipmentWidget->RemoveFromParent();
				PController->SetInputMode(FInputModeGameOnly());
				PController->bShowMouseCursor = false;
				GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Red, TEXT("EquipmentClosed"));
				bIsEquipmentOpened = false;
			}
		}
		else
		{
			FMessageDialog::Debugf(NSLOCTEXT("Error", "Key", "Set Equipment Widget class in Char"));
		}
	}
}

void ADiplomaProjectCharacter::StartAttack()
{
	if (CurrentWeapon)
	{
		CurrentWeapon->StartAttack();
	}
}

void ADiplomaProjectCharacter::StopAttack()
{
	if (CurrentWeapon)
	{
		CurrentWeapon->StopAttack();
	}
}

bool ADiplomaProjectCharacter::IsAlive() const
{
	return HealthComp->GetCurrentAbilityPoints() > 0;
}

void ADiplomaProjectCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void ADiplomaProjectCharacter::MoveRight(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

void ADiplomaProjectCharacter::SetPickupItem(ABasePickup* PickupToSet)
{
	PickupItem = PickupToSet;
}

void ADiplomaProjectCharacter::SetCanInteract(bool bInteract)
{
	bCanInteract = bInteract;
}

UBaseAbilityComponent* ADiplomaProjectCharacter::GetAbilityComponent(EAbilityTypeEnum AbilityType)
{
	switch (AbilityType)
	{
	case EAbilityTypeEnum::DE_Health:
		return HealthComp;
		break;
	case EAbilityTypeEnum::DE_Mana:
		return ManaComp;
		break;
	case EAbilityTypeEnum::DE_Stamina:
		return StaminaComp;
		break;
	default:
		return nullptr;
		UE_LOG(LogTemp, Error, TEXT("Character->GetAbilityComponent returned null!"));
		break;
	}
}

void ADiplomaProjectCharacter::StartSprint()
{
	if (StaminaComp->GetCurrentAbilityPoints() > 3.0)
	{
		if (GetVelocity().Size() > 0)
		{
			StaminaComp->StartLosePoints(3.0);
			GetCharacterMovement()->MaxWalkSpeed = SprintSpeed;
		}
	}
}

void ADiplomaProjectCharacter::StopSprint()
{
	GetCharacterMovement()->MaxWalkSpeed = WalkSpeed;
	StaminaComp->StopLosePoints();
}

void ADiplomaProjectCharacter::Jump()
{
	if (bCanJump)
	{
		if (StaminaComp->GetCurrentAbilityPoints() > 5.0 && !GetCharacterMovement()->IsFalling())
		{
			Super::Jump();
			StaminaComp->RemoveAbilityPoints(5.0);
		}
	}
}

void ADiplomaProjectCharacter::StopJumping()
{
	Super::StopJumping();
}

void ADiplomaProjectCharacter::SaveGame()
{
	SaveGameToSlot();
}


void ADiplomaProjectCharacter::LoadGame()
{
	LoadGameFromSlot();
}

void ADiplomaProjectCharacter::Equip(FName EquipmentSocket, AActor* EquipmentActor)
{
	if (!EquipmentSocket.IsNone() && EquipmentActor)
	{
		EquipmentActor->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, EquipmentSocket);
	}
}

void ADiplomaProjectCharacter::SaveGameToSlot(FString SlotName, int32 UserIndex)
{
	UDiplomaSaveGame* SaveGameInstance = Cast<UDiplomaSaveGame>(UGameplayStatics::CreateSaveGameObject(UDiplomaSaveGame::StaticClass()));

	//Set save game data
	//SaveGameInstance->SaveCharacterTransform(GetActorTransform());
	SaveGameInstance->CharLocation = GetActorLocation();
	SaveGameInstance->InventoryItems = InventoryComp->GetInventoryItemsArray();
	SaveBodyParts(SaveGameInstance);
	SaveAbilityData(SaveGameInstance, HealthComp);
	SaveAbilityData(SaveGameInstance, ManaComp);
	SaveAbilityData(SaveGameInstance, StaminaComp);

	//Save game instance
	UGameplayStatics::SaveGameToSlot(SaveGameInstance, SlotName, UserIndex);
	GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Emerald, TEXT("Game saved to slot ") + SlotName);
}

void ADiplomaProjectCharacter::LoadGameFromSlot(FString SlotName, int32 UserIndex)
{
	UDiplomaSaveGame* LoadGameInstance = Cast<UDiplomaSaveGame>(UGameplayStatics::CreateSaveGameObject(UDiplomaSaveGame::StaticClass()));

	LoadGameInstance = Cast<UDiplomaSaveGame>(UGameplayStatics::LoadGameFromSlot(SlotName, UserIndex));

	InventoryComp->SetInventoryItemsArray(LoadGameInstance->InventoryItems);
	for (ABasePickup* Item : InventoryComp->GetInventoryItemsArray())
	{
		Item->SetPickupUsability(false);
	}
	LoadBodyParts(LoadGameInstance);
	LoadAbilityData(LoadGameInstance, HealthComp);
	LoadAbilityData(LoadGameInstance, ManaComp);
	LoadAbilityData(LoadGameInstance, StaminaComp);

	//SetActorLocation(LoadGameInstance->CharLocation);

	GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Emerald, TEXT("Game Loaded from slot ") + SlotName);
}

void ADiplomaProjectCharacter::SaveBodyParts(UDiplomaSaveGame* GameSaveInstance)
{
	GameSaveInstance->Hair.BodyMesh = HairMesh->SkeletalMesh;
	GameSaveInstance->Face.BodyMesh = FaceMesh->SkeletalMesh;
	GameSaveInstance->Body.BodyMesh = GetMesh()->SkeletalMesh;
	GameSaveInstance->Gloves.BodyMesh = GlovesMesh->SkeletalMesh;
	GameSaveInstance->Shoes.BodyMesh = ShoesMesh->SkeletalMesh;

}

void ADiplomaProjectCharacter::LoadBodyParts(UDiplomaSaveGame* GameSaveInstance)
{
	HairMesh->SetSkeletalMesh(GameSaveInstance->Hair.BodyMesh, false);
	FaceMesh->SetSkeletalMesh(GameSaveInstance->Face.BodyMesh, false);
	GetMesh()->SetSkeletalMesh(GameSaveInstance->Body.BodyMesh, false);
	GlovesMesh->SetSkeletalMesh(GameSaveInstance->Gloves.BodyMesh, false);
	ShoesMesh->SetSkeletalMesh(GameSaveInstance->Shoes.BodyMesh, false);
}

void ADiplomaProjectCharacter::SaveAbilityData(UDiplomaSaveGame* GameSaveInstance, UBaseAbilityComponent* AbilityToSave)
{
	if (AbilityToSave == HealthComp)
	{
		GameSaveInstance->Health.CurrentAbilityPoints = AbilityToSave->GetCurrentAbilityPoints();
		GameSaveInstance->Health.MaxAbilityPoints = AbilityToSave->GetMaxAbilityPoints();
	}
	else if (AbilityToSave == ManaComp)
	{
		GameSaveInstance->Mana.CurrentAbilityPoints = AbilityToSave->GetCurrentAbilityPoints();
		GameSaveInstance->Mana.MaxAbilityPoints = AbilityToSave->GetMaxAbilityPoints();
	}
	else if (AbilityToSave == StaminaComp)
	{
		GameSaveInstance->Stamina.CurrentAbilityPoints = AbilityToSave->GetCurrentAbilityPoints();
		GameSaveInstance->Stamina.MaxAbilityPoints = AbilityToSave->GetMaxAbilityPoints();
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, TEXT("Wrong AbilityComponent ") + AbilityToSave->GetFName().ToString());
	}
}

void ADiplomaProjectCharacter::LoadAbilityData(UDiplomaSaveGame* GameSaveInstance, UBaseAbilityComponent* AbilityToLoad)
{
	if (AbilityToLoad == HealthComp)
	{
		AbilityToLoad->SetCurrentAbilityPoints((GameSaveInstance->Health.CurrentAbilityPoints > 0) ? GameSaveInstance->Health.CurrentAbilityPoints : HealthComp->GetMaxAbilityPoints());
		AbilityToLoad->SetMaxAbilityPoints((GameSaveInstance->Health.MaxAbilityPoints > 0) ? GameSaveInstance->Health.MaxAbilityPoints : HealthComp->GetMaxAbilityPoints());
	}
	else if (AbilityToLoad == ManaComp)
	{
		AbilityToLoad->SetCurrentAbilityPoints((GameSaveInstance->Mana.CurrentAbilityPoints > 0) ? GameSaveInstance->Mana.CurrentAbilityPoints : ManaComp->GetMaxAbilityPoints());
		AbilityToLoad->SetMaxAbilityPoints((GameSaveInstance->Mana.MaxAbilityPoints > 0) ? GameSaveInstance->Mana.MaxAbilityPoints : ManaComp->GetMaxAbilityPoints());
	}
	else if (AbilityToLoad == StaminaComp)
	{
		AbilityToLoad->SetCurrentAbilityPoints((GameSaveInstance->Stamina.CurrentAbilityPoints > 0) ? GameSaveInstance->Stamina.CurrentAbilityPoints : StaminaComp->GetMaxAbilityPoints());
		AbilityToLoad->SetMaxAbilityPoints((GameSaveInstance->Stamina.MaxAbilityPoints > 0) ? GameSaveInstance->Stamina.MaxAbilityPoints : StaminaComp->GetMaxAbilityPoints());
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, TEXT("Wrong AbilityComponent ") + AbilityToLoad->GetFName().ToString());
	}
	if (AbilityToLoad->GetCurrentAbilityPoints() < AbilityToLoad->GetMaxAbilityPoints() && AbilityToLoad->IsRecoverable())
	{
		AbilityToLoad->StartRecover();
	}
}

void ADiplomaProjectCharacter::CameraZoom(float Scale)
{
	if ((Controller != NULL) && (Scale != 0.0f))
	{
		float NewArmLength = CameraBoom->TargetArmLength + Scale * ArmMultiplier;

		if (NewArmLength <= MaxArmLengh && NewArmLength >= MinArmLengh)
		{
			CameraBoom->TargetArmLength = NewArmLength;
		}
	}
}

AEquipableBaseWeapon* ADiplomaProjectCharacter::SpawnWeapon()
{
	if (Weapon_Class)
	{
		FTransform TmpTransform = FTransform::Identity;
		FActorSpawnParameters SpawnParameters;
		SpawnParameters.Owner = this;

		UE_LOG(LogTemp, Warning, TEXT("Trying to spawn wepon"));
		return	Cast<AEquipableBaseWeapon>(GetWorld()->SpawnActor(Weapon_Class, &TmpTransform, SpawnParameters));
	}
	return nullptr;
}

void ADiplomaProjectCharacter::AttachWeapon(FName SocketName)
{
	if (CurrentWeapon)
	{
		CurrentWeapon->SetPickupUsability(true);
		CurrentWeapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, SocketName);
	}
}
