// Fill out your copyright notice in the Description page of Project Settings.


#include "BasePickup.h"
#include <Components/StaticMeshComponent.h>
#include "DiplomaProjectCharacter.h"
#include "InventoryComponent.h"
#include <Kismet/GameplayStatics.h>
#include "Intaction/InterplayActor.h"
#include <Components/BoxComponent.h>
#include <UnrealMathUtility.h>


ABasePickup::ABasePickup()
{
	PickupMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PickupSTMesh"));
	PickupMesh->SetupAttachment(SceneComp);
	PickupMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	PickupMesh->SetMobility(EComponentMobility::Movable);
}

void ABasePickup::OnPickup(class ADiplomaProjectCharacter* Char)
{
	GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Red, GetClass()->GetFName().ToString());
	Char->GetInventoryComponent()->AddItemToInventory(this);
	if (Char->GetInventoryComponent()->IsPickupSuccesful())
	{
		if (PickupSound)
		{
			UGameplayStatics::PlaySoundAtLocation(this, PickupSound, GetActorLocation());
		}
	}
}

void ABasePickup::SetPickupUsability(bool Visibility, ECollisionEnabled::Type NewType)
{
	PickupMesh->SetVisibility(Visibility);
	HitBox->SetCollisionEnabled(NewType);
}

void ABasePickup::OnPickupUse()
{
	OverlappedChar->GetInventoryComponent()->RemoveItemFromInventory(this);
	Destroy();
}

void ABasePickup::OnPickupDrop()
{
	OverlappedChar->GetInventoryComponent()->RemoveItemFromInventory(this);
	FVector SpawnLocation = OverlappedChar->GetActorLocation();
	SpawnLocation.X += FMath::RandRange(-50, 50);
	SpawnLocation.Y += FMath::RandRange(-50, 50);
	SpawnLocation.Z -= 50;
	SetActorLocation(SpawnLocation);
	FRotator SpawnRoation = GetActorRotation();
	SpawnRoation.Yaw = FMath::RandRange(-180, 180);
	SetActorRotation(SpawnRoation);
	SetPickupUsability(true, ECollisionEnabled::QueryOnly);
}

void ABasePickup::BeginPlay()
{
	OverlappedChar = Cast<ADiplomaProjectCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	Super::BeginPlay();
}

void ABasePickup::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	//OverlappedChar = Cast<ADiplomaProjectCharacter>(OtherActor);
	if ((OtherActor != nullptr) && (OtherActor != this) && (OtherActor == OverlappedChar) && (OtherComp != nullptr))
	{
		OverlappedChar->SetCanInteract(true);
		OverlappedChar->SetPickupItem(this);
	}
	Super::OnOverlapBegin(OverlappedComp, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);
}

void ABasePickup::OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if ((OtherActor != nullptr) && (OtherActor != this) && (OtherActor == OverlappedChar) && (OtherComp != nullptr))
	{
		OverlappedChar->SetCanInteract(false);
		OverlappedChar->SetPickupItem(nullptr);
	}
	Super::OnOverlapEnd(OverlappedComp, OtherActor, OtherComp, OtherBodyIndex);
}

