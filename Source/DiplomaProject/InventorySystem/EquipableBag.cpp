// Fill out your copyright notice in the Description page of Project Settings.


#include "EquipableBag.h"
#include "DiplomaProjectCharacter.h"
#include "InventorySystem/InventoryComponent.h"


void AEquipableBag::OnPickupUse()
{
	OverlappedChar->GetInventoryComponent()->SetInventorySize(OverlappedChar->GetInventoryComponent()->GetInventoryMaxSize() + BagSlots);
	Super::OnPickupUse();
}

void AEquipableBag::OnPickupUnEquipped()
{
	OverlappedChar->GetInventoryComponent()->SetInventorySize(OverlappedChar->GetInventoryComponent()->GetInventoryMaxSize() - BagSlots);
	
	int32 CurrentInvSize = OverlappedChar->GetInventoryComponent()->GetInventoryItemsArray().Num();
	int32 MaxInvSize = OverlappedChar->GetInventoryComponent()->GetInventoryMaxSize();
	if (CurrentInvSize > MaxInvSize)
	{
		for (int32 i = CurrentInvSize - 1; i >= MaxInvSize; --i)
		{
			OverlappedChar->GetInventoryComponent()->GetInventoryItemsArray()[i]->OnPickupDrop();
		}
		OnPickupDrop();
	}	
	Super::OnPickupUnEquipped();
}