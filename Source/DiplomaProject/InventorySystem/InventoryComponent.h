// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "BasePickup.h"
#include "InventoryComponent.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE(FNoParamDelegate);
//DECLARE_DYNAMIC_DELEGATE_OneParam(FOneParamDelegate , ABasePickup*);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DIPLOMAPROJECT_API UInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UInventoryComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Inventory)
		int32 InventorySize = 3;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Inventory)
		TArray <ABasePickup*> InventoryMap;


	class ADiplomaProjectCharacter* OwnerChar;


public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	bool bIsPickupSuccesful;
	
	UFUNCTION(BlueprintCallable)
	void	RemoveAllInventoryItems();

	UFUNCTION(BlueprintCallable)
	void	SetInventorySize(int32 NewSize) { InventorySize = NewSize; };

	UFUNCTION(BlueprintPure)
	int32	GetInventorySize(TArray <ABasePickup*> Inventory) const { return Inventory.Num(); };

	UFUNCTION(BlueprintPure)
	int32	GetInventoryMaxSize() const { return InventorySize; };

	UFUNCTION(BlueprintCallable)
	void	AddItemToInventory(ABasePickup* ItemToAdd);
		
	UFUNCTION(BlueprintCallable)
	void	RemoveItemFromInventory(ABasePickup* ItemToRemove);

	UFUNCTION(BlueprintPure)
	TArray <ABasePickup*>	GetInventoryItemsArray() const { return InventoryMap; };

	UFUNCTION(BlueprintCallable)
	void	SetInventoryItemsArray(TArray <ABasePickup*> InventoryItems) { InventoryMap = InventoryItems; };

	UPROPERTY(BlueprintAssignable, Category = Inventory)
	FNoParamDelegate OnInventoryChanged;

	UFUNCTION(BlueprintPure)
	bool IsPickupSuccesful() const { return bIsPickupSuccesful; };

	void	RefreshWidget();
};
