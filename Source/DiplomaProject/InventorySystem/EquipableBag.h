// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "InventorySystem/EquipablePickup.h"
#include "EquipableBag.generated.h"

/**
 * 
 */
UCLASS()
class DIPLOMAPROJECT_API AEquipableBag : public AEquipablePickup
{
	GENERATED_BODY()
	
protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "BagSize")
		int32 BagSlots = 5;

	virtual void OnPickupUse() override;
	virtual void OnPickupUnEquipped() override;
};
