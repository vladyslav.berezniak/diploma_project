// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "InventorySystem/BasePickup.h"
#include "AbilityBoostPickup.generated.h"


UENUM(BlueprintType)
enum class EAbilityEnum : uint8
{
	DE_Health 		UMETA(DisplayName = "Health"),
	DE_Mana 		UMETA(DisplayName = "Mana"),
	DE_Stamina		UMETA(DisplayName = "Stamina")
};

UENUM(BlueprintType)
enum class EAbilityBoosterEnum : uint8
{
	DE_RecoverPoints 		UMETA(DisplayName = "Recover"),
	DE_RegenerationBoost 	UMETA(DisplayName = "Regeneration Boost"),
	DE_MaxPointsBoost		UMETA(DisplayName = "Max Points Boost")
};
/**
 * 
 */
UCLASS()
class DIPLOMAPROJECT_API AAbilityBoostPickup : public ABasePickup
{
	GENERATED_BODY()


protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Ability")
	float BoostPoints;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Ability", meta = (DisplayName = "Boost time in sec"))
	float BoostTime;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Ability")
	class UAnimMontage* PickupUseMontage;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Ability")
	FName SocketToAttach = "hand_lSocket_else";

	UFUNCTION()
	void OnAnimationEnded(UAnimMontage* Montage, bool bInterrupted);

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Ability")
	EAbilityEnum AbilityEnumType;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Ability")
	EAbilityBoosterEnum BoosterEnumType;

	FTimerHandle MaxPointsBoost_TH;
	FTimerHandle RegenBoost_TH;

	float DefaultMaxPoints;
	float DefaultRegenSpeed;

	virtual void OnPickupUse() override;

public:

	UFUNCTION(BlueprintCallable)
	void OnBoost();

	void OnRecover();

	void OnRegenerationBoostStart();

	void OnRegenerationBoostStop();

	void OnMaxPointsBoostStart();

	void OnMaxPointsBoostStop();

	UFUNCTION(BlueprintCallable)
	class UBaseAbilityComponent* GetAbilityToBoost();

};
