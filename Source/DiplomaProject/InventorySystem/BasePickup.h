// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Intaction/InterplayActor.h"
#include "BasePickup.generated.h"

/**
 * 
 */
UCLASS()
class DIPLOMAPROJECT_API ABasePickup : public AInterplayActor
{
	GENERATED_BODY()

		
protected:
	
	class ADiplomaProjectCharacter* OverlappedChar;

public:
	
	ABasePickup();
	
	UFUNCTION(BlueprintCallable)
	void OnPickup(class ADiplomaProjectCharacter* Char);

	void SetPickupUsability(bool Visibility = false, ECollisionEnabled::Type NewType = ECollisionEnabled::NoCollision);

	UFUNCTION(BlueprintCallable)
	virtual void OnPickupUse();

	UFUNCTION(BlueprintCallable)
	virtual void OnPickupDrop();

	/*UFUNCTION(BlueprintCallable)
	virtual void SpawnPickup(FVector Location, FRotator Rotation)*/
	virtual void BeginPlay() override;

	virtual void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;

	virtual void OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) override;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UI")
	class UTexture2D* InventoryIcon;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "PickupItem")
	class UStaticMeshComponent* PickupMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Sound")
	class USoundBase* PickupSound;

};
