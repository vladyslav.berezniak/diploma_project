// Fill out your copyright notice in the Description page of Project Settings.


#include "InventoryComponent.h"
#include "BasePickup.h"
#include "DiplomaProjectCharacter.h"
#include "Ability/BaseAbilityComponent.h"

// Sets default values for this component's properties
UInventoryComponent::UInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...

	
}


// Called when the game starts
void UInventoryComponent::BeginPlay()
{
	Super::BeginPlay();
	 
	// ...
	OwnerChar = Cast<ADiplomaProjectCharacter>(GetOwner());
	OwnerChar->GetAbilityComponent(EAbilityTypeEnum::DE_Health)->OnAbilityZero.AddDynamic(this, &UInventoryComponent::RemoveAllInventoryItems);
}



// Called every frame
void UInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UInventoryComponent::RemoveAllInventoryItems()
{
	if (InventoryMap.Num() > 0)
	{
		for (int32 Index = InventoryMap.Num() - 1; Index >= 0; --Index)
		{
			InventoryMap[Index]->OnPickupDrop();
		}
		RefreshWidget();
	}
}

void UInventoryComponent::AddItemToInventory(ABasePickup* ItemToAdd)
{
	GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Red, ItemToAdd->GetFName().ToString());

	if (InventoryMap.Num() < InventorySize)
	{
		InventoryMap.Emplace(ItemToAdd);
		OnInventoryChanged.Broadcast();
		bIsPickupSuccesful = true;
		ItemToAdd->SetPickupUsability(false);
		RefreshWidget();
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Emerald, TEXT("Inventory Full"));
		bIsPickupSuccesful = false;
	}
}

void UInventoryComponent::RemoveItemFromInventory(ABasePickup* ItemToRemove)
{
	if (InventoryMap.Contains(ItemToRemove))
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, ItemToRemove->GetFName().ToString());
		InventoryMap.RemoveSingle(ItemToRemove);
		RefreshWidget();
	}

}

void UInventoryComponent::RefreshWidget()
{
		OwnerChar->OnInventoryOpen();
		OwnerChar->OnInventoryOpen();
}
