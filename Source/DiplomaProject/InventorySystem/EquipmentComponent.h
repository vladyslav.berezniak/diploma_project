// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "EquipmentComponent.generated.h"

USTRUCT(BlueprintType)
struct FEquipStruct
{
	GENERATED_BODY()

public:
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Basic)
		class AEquipablePickup* RightHand;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Basic)
		class AEquipablePickup* LeftHand;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Basic)
		class AEquipablePickup* Back;

};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DIPLOMAPROJECT_API UEquipmentComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UEquipmentComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Basic)
	FEquipStruct	EquipmentStruct;

	class ADiplomaProjectCharacter* EquipChar;

	UFUNCTION()
	void	RefreshWidget();

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable)
	void	EquipItem(class AEquipablePickup* ItemToEquip);
	
	UFUNCTION(BlueprintCallable)
	void	UnEquipItem(class AEquipablePickup* ItemToUnEquip, bool bAddToInventory = true);

	UFUNCTION(BlueprintCallable)
	void	UnEquipAllItems();
};
