// Fill out your copyright notice in the Description page of Project Settings.


#include "AbilityBoostPickup.h"
#include "BasePickup.h"
#include <MessageDialog.h>
#include "DiplomaProjectCharacter.h"
#include <Animation/AnimMontage.h>
#include <Components/SkeletalMeshComponent.h>
#include "Ability/BaseAbilityComponent.h"
#include <GameFramework/Actor.h>
#include "InventoryComponent.h"

void AAbilityBoostPickup::OnAnimationEnded(UAnimMontage* Montage, bool bInterrupted)
{
	OverlappedChar->GetInventoryComponent()->RemoveItemFromInventory(this);
	OverlappedChar->bCanRoll = true;
	OverlappedChar->bCanJump = true;
	SetPickupUsability(false);
	OnBoost();
}

void AAbilityBoostPickup::OnPickupUse()
{
	if (PickupUseMontage)
	{
		SetPickupUsability(true);
		FOnMontageEnded EndDelegate;
		EndDelegate.BindUObject(this, &AAbilityBoostPickup::OnAnimationEnded);
		OverlappedChar->bCanRoll = false;
		OverlappedChar->bCanJump = false;
		OverlappedChar->GetMesh()->GetAnimInstance()->Montage_Play(PickupUseMontage);
		OverlappedChar->GetMesh()->GetAnimInstance()->Montage_Play(PickupUseMontage);
		OverlappedChar->GetMesh()->GetAnimInstance()->Montage_SetEndDelegate(EndDelegate);
		if (SocketToAttach != NAME_None)
		{
			PickupMesh->AttachToComponent(OverlappedChar->GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, SocketToAttach);
			GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Green, SocketToAttach.ToString());
		}
		else
		{
			FMessageDialog::Debugf(NSLOCTEXT("Error", "Key", "Set Ability Pickup Socket"));
		}
	}
	else
	{
		OnBoost();
	}
}

void AAbilityBoostPickup::OnBoost()
{
	switch (BoosterEnumType)
	{
	case EAbilityBoosterEnum::DE_RecoverPoints:
		OnRecover();
		break;
	case EAbilityBoosterEnum::DE_RegenerationBoost:
		OnRegenerationBoostStart();
		break;
	case EAbilityBoosterEnum::DE_MaxPointsBoost:
		OnMaxPointsBoostStart();
		break;
	default:
		break;
	}
}

void AAbilityBoostPickup::OnRecover()
{
	GetAbilityToBoost()->AddAbilityPoints(BoostPoints);
	Destroy();
}

void AAbilityBoostPickup::OnRegenerationBoostStart()
{
	DefaultRegenSpeed = GetAbilityToBoost()->GetRecoverStepPoints();
	GetAbilityToBoost()->SetRecoverStepPoints(BoostPoints);
	GetWorldTimerManager().SetTimer(RegenBoost_TH, this, &AAbilityBoostPickup::OnRegenerationBoostStop, 1.f, false, BoostTime);
}

void AAbilityBoostPickup::OnRegenerationBoostStop()
{
	GetAbilityToBoost()->SetRecoverStepPoints(DefaultRegenSpeed);
	GetWorldTimerManager().ClearTimer(RegenBoost_TH);
	Destroy();
}

void AAbilityBoostPickup::OnMaxPointsBoostStart()
{
	DefaultMaxPoints = GetAbilityToBoost()->GetMaxAbilityPoints();
	GetAbilityToBoost()->SetMaxAbilityPoints(BoostPoints);
	GetAbilityToBoost()->SetCurrentAbilityPoints(BoostPoints);
	GetWorldTimerManager().SetTimer(MaxPointsBoost_TH, this, &AAbilityBoostPickup::OnMaxPointsBoostStop, 1.f, false, BoostTime);
}

void AAbilityBoostPickup::OnMaxPointsBoostStop()
{
	if (GetAbilityToBoost()->GetCurrentAbilityPoints() > DefaultMaxPoints)
	{
		GetAbilityToBoost()->SetCurrentAbilityPoints(DefaultMaxPoints);
	}
	GetAbilityToBoost()->SetMaxAbilityPoints(DefaultMaxPoints);
	GetWorldTimerManager().ClearTimer(MaxPointsBoost_TH);
	Destroy();
}

UBaseAbilityComponent* AAbilityBoostPickup::GetAbilityToBoost()
{
	switch (AbilityEnumType)
	{
	case EAbilityEnum::DE_Health:
		return OverlappedChar->GetAbilityComponent(EAbilityTypeEnum::DE_Health);
		break;
	case EAbilityEnum::DE_Mana:
		return OverlappedChar->GetAbilityComponent(EAbilityTypeEnum::DE_Mana);
		break;
	case EAbilityEnum::DE_Stamina:
		return OverlappedChar->GetAbilityComponent(EAbilityTypeEnum::DE_Stamina);
		break;
	default:
		return nullptr;
		UE_LOG(LogTemp, Error, TEXT("AAbilityBoostPickup::GetAbilityToBoost() returned nullptr!"));
		break;
	}
}
