// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "InventorySystem/BasePickup.h"
#include "EquipablePickup.generated.h"

/**
 * 
 */

UENUM(BlueprintType)
enum class EEquipTypeEnum : uint8
{
	DE_RightHand 			UMETA(DisplayName = "RightHand"),
	DE_LeftHand 			UMETA(DisplayName = "LeftHand"),
	DE_Back					UMETA(DisplayName = "Back")
};

UCLASS()
class DIPLOMAPROJECT_API AEquipablePickup : public ABasePickup
{
	GENERATED_BODY()

public:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickup")
	EEquipTypeEnum EquipTypeEnum;
	
	UFUNCTION(BlueprintCallable)
	virtual void OnPickupUnEquipped();

	virtual void OnPickupUse() override;
	virtual void OnPickupDrop() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickup")
	FName AttachmentSocket;

protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickup")
	class UAnimMontage* EquipMontage;
	
	
};
 