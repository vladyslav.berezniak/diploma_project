// Fill out your copyright notice in the Description page of Project Settings.


#include "EquipablePickup.h"
#include "BasePickup.h"
#include "DiplomaProjectCharacter.h"
#include "EquipmentComponent.h"
#include <Animation/AnimInstance.h>
#include "InventoryComponent.h"

void AEquipablePickup::OnPickupUse()
{
	if (EquipMontage)
	{
		OverlappedChar->GetMesh()->GetAnimInstance()->Montage_Play(EquipMontage);
	}
	//OverlappedChar->GetMesh()->sock
	OverlappedChar->Equip(AttachmentSocket, this);
	OverlappedChar->GetEquipmentComponent()->EquipItem(this);
	SetPickupUsability(true, ECollisionEnabled::NoCollision);
}

void AEquipablePickup::OnPickupDrop()
{
	DetachFromActor(FDetachmentTransformRules::KeepRelativeTransform);
	Super::OnPickupDrop();
}

void AEquipablePickup::OnPickupUnEquipped()
{
	OverlappedChar->GetInventoryComponent()->RefreshWidget();
}
