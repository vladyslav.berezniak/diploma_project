// Fill out your copyright notice in the Description page of Project Settings.


#include "EquipmentComponent.h"
#include "InventoryComponent.h"
#include "DiplomaProjectCharacter.h"
#include "EquipablePickup.h"
#include "Ability/BaseAbilityComponent.h"

// Sets default values for this component's properties
UEquipmentComponent::UEquipmentComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}


// Called when the game starts
void UEquipmentComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	EquipChar = Cast<ADiplomaProjectCharacter>(GetOwner());
	EquipChar->GetAbilityComponent(EAbilityTypeEnum::DE_Health)->OnAbilityZero.AddDynamic(this, &UEquipmentComponent::UnEquipAllItems);
}



// Called every frame
void UEquipmentComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UEquipmentComponent::EquipItem(class AEquipablePickup* ItemToEquip)
{
	switch (ItemToEquip->EquipTypeEnum)
	{
	case EEquipTypeEnum::DE_RightHand:
		if (EquipmentStruct.RightHand != nullptr)
		{
			UnEquipItem(EquipmentStruct.RightHand);
		}
		EquipmentStruct.RightHand = ItemToEquip;
		break;
	case EEquipTypeEnum::DE_LeftHand:
		if (EquipmentStruct.LeftHand != nullptr)
		{
			UnEquipItem(EquipmentStruct.LeftHand);
		}
		EquipmentStruct.LeftHand = ItemToEquip;
		break;
	case EEquipTypeEnum::DE_Back:
		if (EquipmentStruct.Back != nullptr)
		{
			UnEquipItem(EquipmentStruct.Back);
		}
		EquipmentStruct.Back = ItemToEquip;
		break;
	default:
		GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Red, TEXT("Wrong Equipment Type"));
		break;
	}
	RefreshWidget();
	EquipChar->GetInventoryComponent()->RemoveItemFromInventory(ItemToEquip);
	GEngine->AddOnScreenDebugMessage(-1, 10.0f, FColor::Red, ItemToEquip->GetFName().ToString() + TEXT(" Equipped"));
}

void UEquipmentComponent::UnEquipItem(class AEquipablePickup* ItemToUnEquip, bool bAddToInventory)
{
	if (ItemToUnEquip)
	{
		EquipChar->GetInventoryComponent()->AddItemToInventory(ItemToUnEquip);
		//ItemToUnEquip->SetPickupUsability(false);
		
		if (EquipChar->GetInventoryComponent()->bIsPickupSuccesful)
		{
			ItemToUnEquip->OnPickupUnEquipped();
			switch (ItemToUnEquip->EquipTypeEnum)
			{
			case EEquipTypeEnum::DE_RightHand:
				EquipmentStruct.RightHand = nullptr;
				break;
			case EEquipTypeEnum::DE_LeftHand:
				EquipmentStruct.LeftHand = nullptr;
				break;
			case EEquipTypeEnum::DE_Back:
				EquipmentStruct.Back = nullptr;
				break;
			default:
				break;
			}
		}

		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, ItemToUnEquip->GetFName().ToString() + TEXT(" UnEquipped"));
		RefreshWidget();
	}
}

void UEquipmentComponent::UnEquipAllItems()
{
	UnEquipItem(EquipmentStruct.Back, false);
	UnEquipItem(EquipmentStruct.RightHand, false);
	UnEquipItem(EquipmentStruct.LeftHand, false);
	EquipChar->GetInventoryComponent()->RemoveAllInventoryItems();
}

void UEquipmentComponent::RefreshWidget()
{
	EquipChar->OnEquipmentOpen();
	EquipChar->OnEquipmentOpen();
}
