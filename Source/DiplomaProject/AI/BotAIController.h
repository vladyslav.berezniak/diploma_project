// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "BotAIController.generated.h"

/**
 * 
 */
UCLASS()
class DIPLOMAPROJECT_API ABotAIController : public AAIController
{
	GENERATED_BODY()

private:

	UPROPERTY(EditDefaultsOnly)
		class	UBlackboardComponent* BlackboardComp;

	/* Cached BT component */
	UPROPERTY(EditDefaultsOnly)
		class	UBehaviorTreeComponent* BehaviorComp;

public:

	ABotAIController();

	UFUNCTION(BlueprintPure)
		UBlackboardComponent* GetBlackboardComp() const { return BlackboardComp; }

	UFUNCTION(BlueprintPure)
		UBehaviorTreeComponent* GetBehaviorComp() const { return BehaviorComp; }

	virtual void UpdateControlRotation(float DeltaTime, bool bUpdatePawn = true) override;

	UFUNCTION(BlueprintPure)
		class ADiplomaProjectCharacter* GetEnemy() const;

	UFUNCTION(BlueprintCallable)
		class AActor* FindClosestActor(TArray<AActor*> ActorsArr, AActor* TargetActor);


protected:
	virtual void OnPossess(class APawn* InPawn) override;
	virtual void OnUnPossess() override;
	
	int32 EnemyKeyID;
	int32 LocationKeyID;

};
