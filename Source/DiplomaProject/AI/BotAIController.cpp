// Fill out your copyright notice in the Description page of Project Settings.


#include "BotAIController.h"
#include "BotAI.h"
#include <BehaviorTree/BlackboardComponent.h>
#include <BehaviorTree/BehaviorTreeComponent.h>
#include <BehaviorTree/Blackboard/BlackboardKeyType_Object.h>
#include "BehaviorTree/BehaviorTree.h"

ABotAIController::ABotAIController()
{
	BlackboardComp = CreateDefaultSubobject<UBlackboardComponent>(TEXT("BlackBoardComp"));

	BehaviorComp = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("BehaviorComp"));

	BrainComponent = BehaviorComp;
}

void ABotAIController::UpdateControlRotation(float DeltaTime, bool bUpdatePawn)
{
	// Look toward focus / look to enemy 
	FVector TargetPoint = (GetEnemy()) ? GetEnemy()->GetActorLocation() : GetFocalPoint();

	if (!TargetPoint.IsZero() && GetPawn())
	{
		FVector Direction = TargetPoint - GetPawn()->GetActorLocation();
		FRotator NewControlRotation = Direction.Rotation();
		NewControlRotation.Yaw = FRotator::ClampAxis(NewControlRotation.Yaw);

		SetControlRotation(NewControlRotation);

		APawn* AIPawn = GetPawn();
		if (AIPawn && bUpdatePawn)
		{
			AIPawn->FaceRotation(NewControlRotation, DeltaTime);
		}
	}
}


ADiplomaProjectCharacter* ABotAIController::GetEnemy() const
{
	if (BlackboardComp)
	{
		return Cast<ADiplomaProjectCharacter>(BlackboardComp->GetValue<UBlackboardKeyType_Object>(EnemyKeyID));
	}
	return nullptr;
}

AActor* ABotAIController::FindClosestActor(TArray<AActor*> ActorsArr, AActor* TargetActor)
{
	AActor* ClosestActor = nullptr;
	float ClosestDistSqrd = MAX_FLT, MinDistance = 0.f;
	for (AActor* ActorItr : ActorsArr)
	{
		if (!ActorItr || ActorItr->IsPendingKill()) { continue; }

		const float DistSqrd = FVector(ActorItr->GetActorLocation() - TargetActor->GetActorLocation()).SizeSquared();
		if (DistSqrd < ClosestDistSqrd && DistSqrd > MinDistance)
		{
			ClosestDistSqrd = DistSqrd;
			ClosestActor = ActorItr;
		}
	}
	return ClosestActor;
}

void ABotAIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	ABotAI* Bot = Cast<ABotAI>(InPawn);

	// start behavior
	if (Bot && Bot->BotBehavior)
	{
		if (Bot->BotBehavior->BlackboardAsset)
		{
			BlackboardComp->InitializeBlackboard(*Bot->BotBehavior->BlackboardAsset);
		}

		EnemyKeyID = BlackboardComp->GetKeyID("Enemy");
		LocationKeyID = BlackboardComp->GetKeyID("Location");

		BehaviorComp->StartTree(*(Bot->BotBehavior));
	}
}

void ABotAIController::OnUnPossess()
{
	Super::OnUnPossess();

	BehaviorComp->StopTree();
}


