// Fill out your copyright notice in the Description page of Project Settings.


#include "BotAI.h"
#include <Perception/AISenseConfig_Sight.h>
#include <Perception/AIPerceptionComponent.h>
#include "Weapon/EquipableBaseWeapon.h"
#include "InventorySystem/EquipmentComponent.h"
#include <Kismet/KismetMathLibrary.h>

ABotAI::ABotAI()
{
	//AIControllerClass = ABotAIController::StaticClass();

	bUseControllerRotationYaw = true;

	SightConfig = CreateDefaultSubobject<UAISenseConfig_Sight>(TEXT("AISightConfig"));
	SightConfig->DetectionByAffiliation.bDetectEnemies = true;
	SightConfig->DetectionByAffiliation.bDetectFriendlies = true;
	SightConfig->DetectionByAffiliation.bDetectNeutrals = false;
	SightConfig->SightRadius = AISightRadius;
	SightConfig->LoseSightRadius = AILoseSightRadius;
	SightConfig->PeripheralVisionAngleDegrees = AIPeripheralSightAngle;
	SightConfig->SetMaxAge(AISightAge);

	AIPerceptionComp = CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("AIPerception"));
	AIPerceptionComp->ConfigureSense(*SightConfig);
	AIPerceptionComp->SetDominantSense(SightConfig->GetSenseImplementation());

	if (GetMesh())
	{
		GetMesh()->AddLocalTransform(FTransform(FRotator(0, -90, 0).Quaternion(), FVector(0, 0, -95)));
	}
}

void ABotAI::OnPlayerDied()
{
	if (ensure(CurrentWeapon))
	{
		ABasePickup* SpawnedWeapon = GetWorld()->SpawnActor<ABasePickup>(CurrentWeapon->GetClass(), GetActorLocation(), GetActorRotation());
		CurrentWeapon->Destroy();
		if (bSpawnPickupsOnDeath)
		{
			SpawnPickup(GetActorLocation(), GetActorRotation());
		}
	}
	SetLifeSpan(1.f);
}

void ABotAI::SpawnPickup(FVector Loc, FRotator Rot)
{
	FActorSpawnParameters SpawnParams;
	if (PickupsSpawnMap.Num() > 0)
	{
		for (auto& Elem : PickupsSpawnMap)
		{
			Loc.X += FMath::RandRange(50.0f, 50.0f);
			Loc.Y += FMath::RandRange(50.0f, 50.0f);
			if (ShouldSpawn(Elem.Value))
			{
				ABasePickup* SpawnedPickupRef = GetWorld()->SpawnActor<ABasePickup>(Elem.Key, Loc, Rot, SpawnParams);
			}
		}
	}
}

bool ABotAI::ShouldSpawn(float Chance)
{
	return (UKismetMathLibrary::RandomBoolWithWeight(Chance == 1) ? true : false);
}

void ABotAI::FaceRotation(FRotator NewRotation, float DeltaTime)
{
	FRotator CurrentRotation = FMath::RInterpTo(GetActorRotation(), NewRotation, DeltaTime, 8.0f);

	Super::FaceRotation(CurrentRotation, DeltaTime);
}

void ABotAI::OnInteract()
{
}

void ABotAI::BeginPlay()
{
	/*CurrentWeapon = SpawnWeapon();
	AttachWeapon(CurrentWeapon->AttachmentSocket);*/
	Super::BeginPlay();
}

