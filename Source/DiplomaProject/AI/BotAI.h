// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "DiplomaProjectCharacter.h"
#include "BotAI.generated.h"

/**
 * 
 */
UCLASS()
class DIPLOMAPROJECT_API ABotAI : public ADiplomaProjectCharacter
{
	GENERATED_BODY()

public:

	ABotAI();

	UPROPERTY(EditAnywhere, Category = "Behavior")
	class UBehaviorTree* BotBehavior;

	virtual	void OnPlayerDied() override;

	UFUNCTION(BlueprintCallable)
		void SpawnPickup(FVector Loc, FRotator Rot);
protected:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class	UAIPerceptionComponent* AIPerceptionComp;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class	UAISenseConfig_Sight* SightConfig;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		float	AISightRadius = 2000.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		float	AILoseSightRadius = 2500.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		float	AISightAge = 10.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		float	AIPeripheralSightAngle = 65.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickups")
		bool bSpawnPickupsOnDeath = true;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Pickups", meta = (EditCondition = "bSpawnOnDeath"))
		TMap<TSubclassOf<ABasePickup>, float> PickupsSpawnMap;

	bool ShouldSpawn(float Chance);

	virtual void FaceRotation(FRotator NewRotation, float DeltaTime = 0.f) override;
	
	virtual void OnInteract() override;

	virtual void BeginPlay() override;

};


