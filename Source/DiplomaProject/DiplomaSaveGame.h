// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "DiplomaSaveGame.generated.h"

/**
 * 
 */
USTRUCT(BlueprintType)
struct FBodyStruct 
{
	GENERATED_BODY()
public:
	UPROPERTY(VisibleAnywhere, Category = Basic)
		class USkeletalMesh* BodyMesh;

};

USTRUCT(BlueprintType)
struct FAbilityStruct
{
	GENERATED_BODY()

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Ability")
	float MaxAbilityPoints;

	UPROPERTY(EditDefaultsOnly, Category = "Ability")
	float CurrentAbilityPoints;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Ability")
	float RecoverStep;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Ability")
	float RecoverDelay;
};

UCLASS()
class DIPLOMAPROJECT_API UDiplomaSaveGame : public USaveGame
{
	GENERATED_BODY()
	
public:

	UDiplomaSaveGame();

	UPROPERTY(VisibleAnywhere, Category = Basic)
	FBodyStruct Hair;
	
	UPROPERTY(VisibleAnywhere, Category = Basic)
	FBodyStruct Face;
	
	UPROPERTY(VisibleAnywhere, Category = Basic)
	FBodyStruct Body;
	
	UPROPERTY(VisibleAnywhere, Category = Basic)
	FBodyStruct Gloves;
	
	UPROPERTY(VisibleAnywhere, Category = Basic)
	FBodyStruct Shoes;

	UPROPERTY(VisibleAnywhere, Category = Basic)
	FAbilityStruct Health;

	UPROPERTY(VisibleAnywhere, Category = Basic)
	FAbilityStruct Stamina;

	UPROPERTY(VisibleAnywhere, Category = Basic)
	FAbilityStruct Mana;
	
	UPROPERTY(VisibleAnywhere, Category = Basic)
	FString SaveSlotName;

	UPROPERTY(VisibleAnywhere, Category = Basic)
	uint32 UserIndex;

	UPROPERTY(VisibleAnywhere, Category = Basic)
	FTransform CharTransform;
	
	UPROPERTY(VisibleAnywhere, Category = Basic)
	FVector CharLocation;

	UPROPERTY(VisibleAnywhere, Category = Basic)
	TArray<class ABasePickup*> InventoryItems;

	UPROPERTY(VisibleAnywhere, Category = Basic)
	TArray<class USkeletalMesh*> Skelet;

	UFUNCTION(BlueprintCallable)
	void	SaveCharacterTransform(FTransform CharLastTransform) { CharTransform = CharLastTransform; };

	UFUNCTION(BlueprintCallable)
	void	SaveCharacterLocation(FVector CharLastLocation) { CharLocation = CharLastLocation; };
	
	UFUNCTION(BlueprintCallable)
	FTransform	GetCharacterTrancform() const { return CharTransform; };

	UFUNCTION(BlueprintCallable)
	FVector	GetCharacterLocation() const { return CharLocation; };
};
