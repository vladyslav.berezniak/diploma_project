// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include <Engine/DataTable.h>
#include "SkelMeshStruct.generated.h"

/**
 * 
 */
USTRUCT(BlueprintType)
struct FSkelMeshStruct : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Character)
	TArray	<class USkeletalMesh*> SkelMeshArray;
		
	class ADiplomaProjectCharacter* BaseChar;

	//UPROPERTY(BlueprintCallable)
	//void SetMesh(int MeshIndex);

	FSkelMeshStruct();
};
