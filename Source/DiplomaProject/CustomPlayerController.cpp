// Fill out your copyright notice in the Description page of Project Settings.

#include "DiplomaProject.h"
#include "CustomPlayerController.h"
#include "GameFramework/PlayerState.h"


void ACustomPlayerController::ChangeStateTo_Spectator()
{
	ChangeState(NAME_Spectating);
	if (PlayerState != NULL)
	{
		PlayerState->bIsSpectator = true;
		PlayerState->bOnlySpectator = true;
	}
}

void ACustomPlayerController::ChangeStateTo_Player()
{
	ChangeState(NAME_Playing);
	if (PlayerState != NULL)
	{
		PlayerState->bIsSpectator = false;
		PlayerState->bOnlySpectator = false;
	}
}
