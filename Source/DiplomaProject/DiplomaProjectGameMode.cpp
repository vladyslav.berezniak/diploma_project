// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "DiplomaProjectGameMode.h"
#include "DiplomaProjectCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "CustomPlayerController.h"
#include <Kismet/GameplayStatics.h>
#include "Ability/BaseAbilityComponent.h"

ADiplomaProjectGameMode::ADiplomaProjectGameMode()
{

	PlayerControllerClass = ACustomPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	/*static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}*/
}

void ADiplomaProjectGameMode::BeginPlay()
{
	Super::BeginPlay();
}

